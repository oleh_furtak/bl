import os
import requests
from .models import EmployeeExtended
from django.http import JsonResponse
from django.core import serializers
from django.http import HttpResponse


def get_employees():
    employees_exd_info = list(EmployeeExtended.objects.values('tabnumber', 'avatar', 'mobilephone_number'))
    return employees_exd_info


def get_employee(tabnumber):
    def employee_to_dictionary(employee):
        if employee == None:
            return None

        dictionary = {}
        dictionary["tabnumber"] = employee.tabnumber
        dictionary["avatar"] = employee.avatar
        dictionary["mobilephone_number"] = employee.mobilephone_number
        dictionary["fb_link"] = employee.fb_link
        dictionary["linkedin_link"] = employee.linkedin_link
        dictionary["instagram_link"] = employee.instagram_link
        return dictionary

    employee = EmployeeExtended.objects.get(tabnumber=tabnumber)
    employee_to_js = employee_to_dictionary(employee)
    return employee_to_js


def get_employees_detail(id):
    url = f"http://10.1.14.28:8000/api/v1/hr/Employee/{id}/"
    headers = {
        "Authorization": "Token 00b68118a3f509b36d503ba16913ab08fef84c36"
    }
    res = requests.get(url, headers=headers)
    employee = res.json()
    return employee


def get_employees_list():
    url = 'http://10.1.14.28:8000/api/v1/hr/Employee/?employee_status_id=2'
    headers = {
        "Authorization": "Token 00b68118a3f509b36d503ba16913ab08fef84c36"
    }
    res = requests.get(url, headers=headers)
    employees_list = res.json()
    return employees_list


def get_employees_filtered_list(search_string, division_id):

    try:
        int(division_id)
        div_id = division_id
        print(type(division_id))
    except:
        print('IS NOT A NUMBER')
        div_id = ''

    print(div_id)

    url_str = f'full_name={search_string}&division_id={div_id}&employee_status_id=2'
    print(url_str)

    url = f'http://10.1.14.28:8000/api/v1/hr/Employee/?{url_str}'
    print(url)
    headers = {
        "Authorization": "Token 00b68118a3f509b36d503ba16913ab08fef84c36"
    }
    res = requests.get(url, headers=headers)
    filtered_records = res.json()
    return filtered_records


def get_departments_list():
    url = 'http://10.1.14.28:8000/api/v1/hr/Division/'
    headers = {
        "Authorization": "Token 00b68118a3f509b36d503ba16913ab08fef84c36"
    }
    res = requests.get(url, headers=headers)
    departments = res.json()
    return departments
