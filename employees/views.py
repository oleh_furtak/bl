from django.http import HttpResponse
from django.shortcuts import render
from .services import get_employees_list, get_employees, get_employee, get_employees_detail, get_departments_list, get_employees_filtered_list


def index(request):
    return HttpResponse("Список працівників")


def employee_detail(request, detail):
    template_name = 'employee_detail.html'
    return render(request, template_name, {
            'employee': get_employees_detail(detail)
        })

def list_filtered(request):
    template_name = 'employees_list.html'
    query_dict = (request.GET)
    print(query_dict)
    search_query = query_dict.get("full_name")
    division_id = query_dict.get("division_id")
    return render(request, template_name, {
            'employees': get_employees_filtered_list(search_query, division_id),
            'employees_extended': get_employees(),
            'departments': get_departments_list()
        })


def employees_list(request):
    template_name = 'employees_list.html'
    return render(request, template_name, {
            'employees': get_employees_list(),
            'employees_extended': get_employees(),
            'departments': get_departments_list()
        })

