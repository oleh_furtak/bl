from django.urls import path
from . import views
from bl import settings
from django.conf.urls.static import static


app_name = "employees"

urlpatterns = [
    path('', views.index, name='index'),
    path('list/', views.employees_list, name='list'),
    path('list/search/', views.list_filtered, name='list_filtered'),
    path('detail/<int:detail>', views.employee_detail, name='detail')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)