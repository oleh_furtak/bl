from django.db import models
from django.core.validators import RegexValidator
import uuid
import os

def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    path_exd = instance.tabnumber
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join("avatars", path_exd, filename)

class EmployeeExtended(models.Model):
    tabnumber = models.CharField(max_length=14)
    avatar = models.ImageField(upload_to=get_file_path)
    fb_link = models.URLField(max_length=250, blank=True, default='https://www.facebook.com/')
    linkedin_link = models.URLField(max_length=250, blank=True, default='https://www.linkedin.com/')
    instagram_link = models.URLField(max_length=250, blank=True, default='https://www.instagram.com/')
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Телефонний номер має бути введений у форматі: '+380999999999'.")
    mobilephone_number = models.CharField(validators=[phone_regex], max_length=13, blank=True)

    class Meta:
        ordering = ('tabnumber',)
        verbose_name = 'EmployeeExtended'
        verbose_name_plural = 'EmployeesExtended'

    def __str__(self):
        return self.tabnumber
