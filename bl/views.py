from django.http import HttpResponse
from django.views.generic import TemplateView
from django.shortcuts import render


def home(request):
    template_name = 'home_page.html'
    return render(request, template_name, {
        })