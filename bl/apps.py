from django.apps import AppConfig


class BlAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'BL'
