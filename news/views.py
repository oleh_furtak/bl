from django.http import HttpResponse
from django.views.generic import TemplateView
from django.shortcuts import render


def news_list(request):
    template_name = 'news_list.html'
    return render(request, template_name, {})