from django.urls import path
from . import views
from bl import settings
from django.conf.urls.static import static

app_name = "news"

urlpatterns = [
    path('list', views.news_list, name='list'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)